package you.dev.domain.loyalties.model

data class TargetRestaurant(val id: String, val name: String, val address: String)

data class UserDiscount(val idUser: String, val amount: Double, val restaurant: TargetRestaurant)
