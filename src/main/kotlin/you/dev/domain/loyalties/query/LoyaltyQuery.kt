package you.dev.domain.loyalties.query

import you.dev.core.query.Query

open class LoyaltyQuery : Query {
    override fun getDomainName(): String = "loyalties"
}
