package you.dev.domain.loyalties.query

import you.dev.core.query.local.LocalQueryBus
import you.dev.core.query.QueryHandler
import you.dev.domain.loyalties.model.TargetRestaurant
import you.dev.domain.loyalties.model.UserDiscount
import you.dev.domain.payments.model.Payment
import you.dev.domain.payments.query.PaymentsByUserQuery
import you.dev.domain.restaurants.model.RestaurantDetails
import you.dev.domain.restaurants.query.RestaurantDetailsQuery

class DiscountsByUserQueryHandler(val queryBus: LocalQueryBus): QueryHandler<DiscountsByUserQuery, List<UserDiscount>> {
    override fun execute(query: DiscountsByUserQuery): List<UserDiscount> {
        val payments = queryBus.dispatch<PaymentsByUserQuery, List<Payment>>(PaymentsByUserQuery(query.idUser))
        val lastPayment = payments.last()
        val restaurantDetails: RestaurantDetails.Info = queryBus.dispatch(RestaurantDetailsQuery(lastPayment.idRestaurant))
        val restaurant = restaurantDetails.toTargetRestaurant()

        return if (lastPayment.amount > 100) listOf(UserDiscount(query.idUser, 10.0, restaurant))
        else listOf(UserDiscount(query.idUser, 5.0, restaurant))
    }

}

private fun RestaurantDetails.Info.toTargetRestaurant() = TargetRestaurant(id, name, address)
