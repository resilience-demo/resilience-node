package you.dev.domain.loyalties.query

data class DiscountsByUserQuery(val idUser: String) : LoyaltyQuery()
