package you.dev.domain.loyalties.route

import org.http4k.contract.contract
import org.http4k.contract.openapi.ApiInfo
import org.http4k.contract.openapi.v3.OpenApi3
import org.http4k.format.Jackson
import you.dev.core.query.local.LocalQueryBus

fun loyaltyRouter(queryBus: LocalQueryBus) = contract {
    renderer = OpenApi3(ApiInfo("Fidelity API", "v1.0"), Jackson)
    descriptionPath = "/swagger.json"
    routes += DiscountsByUserRoute(queryBus)()
}
