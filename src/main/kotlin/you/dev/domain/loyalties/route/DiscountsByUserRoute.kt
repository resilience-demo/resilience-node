package you.dev.domain.loyalties.route

import org.http4k.contract.ContractRoute
import org.http4k.contract.meta
import org.http4k.core.*
import org.http4k.core.Method.GET
import org.http4k.core.Status.Companion.OK
import org.http4k.format.Jackson.auto
import you.dev.core.query.local.LocalQueryBus
import you.dev.domain.loyalties.model.TargetRestaurant
import you.dev.domain.loyalties.model.UserDiscount
import you.dev.domain.loyalties.query.DiscountsByUserQuery

private val queryLens = Body.auto<DiscountsByUserQuery>().toLens()
private val responseLens = Body.auto<List<UserDiscount>>().toLens()

class DiscountsByUserRoute(private val queryBus: LocalQueryBus) {
    private val spec = "/discounts" meta {
        summary = "list of discounts for a user at a specific time"
        receiving(queryLens to DiscountsByUserQuery("u1"))
        returning(
            OK, responseLens to listOf(
                UserDiscount(
                    "u1",
                    10.0,
                    TargetRestaurant("r1", "au bon coin", "juste au coin")
                )
            )
        )
    } bindContract GET

    private val discounts: HttpHandler = { request: Request ->
        val query: DiscountsByUserQuery = queryLens(request)
        val response: List<UserDiscount> = queryBus.dispatch(query)
        Response(OK).with(responseLens of response)
    }

    operator fun invoke(): ContractRoute = spec to discounts
}
