package you.dev.domain.restaurants.query

data class RestaurantDetailsQuery(val id: String) : RestaurantQuery()
