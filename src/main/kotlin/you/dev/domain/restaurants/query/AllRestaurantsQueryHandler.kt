package you.dev.domain.restaurants.query

import you.dev.core.query.QueryHandler
import you.dev.domain.restaurants.model.RestaurantDetails
import you.dev.domain.restaurants.model.RestaurantsRepository

class AllRestaurantsQueryHandler(private val repository: RestaurantsRepository) :
    QueryHandler<AllRestaurantsQuery, List<RestaurantDetails>> {

    override fun execute(query: AllRestaurantsQuery): List<RestaurantDetails> {
        return repository.getAll()
    }

}
