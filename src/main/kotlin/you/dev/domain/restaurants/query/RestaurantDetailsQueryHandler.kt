package you.dev.domain.restaurants.query

import you.dev.core.query.QueryHandler
import you.dev.domain.restaurants.model.RestaurantDetails
import you.dev.domain.restaurants.model.RestaurantsRepository

class RestaurantDetailsQueryHandler(private val repository: RestaurantsRepository) :
    QueryHandler<RestaurantDetailsQuery, RestaurantDetails> {

    override fun execute(query: RestaurantDetailsQuery): RestaurantDetails {
        return repository.get(query.id)
    }
}
