package you.dev.domain.restaurants.query

import you.dev.core.query.Query

open class RestaurantQuery: Query {
    override fun getDomainName(): String = "restaurants"
}
