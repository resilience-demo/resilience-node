package you.dev.domain.restaurants.model

sealed class RestaurantDetails {
    data class Info(val id: String, val name: String, val address: String) : RestaurantDetails()
    object NotExisting : RestaurantDetails()
}
