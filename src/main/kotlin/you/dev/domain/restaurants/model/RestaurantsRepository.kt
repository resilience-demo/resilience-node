package you.dev.domain.restaurants.model

class RestaurantsRepository {
    private val restaurants: Map<String, RestaurantDetails> = mapOf(
        "r1" to RestaurantDetails.Info("r1", "Au bon coin", "au croisement du coin"),
        "r2" to RestaurantDetails.Info("r2", "A l'autre coin", "en face d'au bon coin"),
        "r3" to RestaurantDetails.Info("r3", "Sur cour", "derrière la porte cochère")
    )

    fun get(id: String): RestaurantDetails {
        return restaurants[id] ?: RestaurantDetails.NotExisting
    }

    fun getAll(): List<RestaurantDetails> {
        return restaurants.values.toList()
    }
}
