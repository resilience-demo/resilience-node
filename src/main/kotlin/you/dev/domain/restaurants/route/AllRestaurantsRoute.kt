package you.dev.domain.restaurants.route

import org.http4k.contract.ContractRoute
import org.http4k.contract.meta
import org.http4k.core.*
import org.http4k.core.Method.GET
import org.http4k.core.Status.Companion.OK
import org.http4k.format.Jackson.auto
import you.dev.core.query.local.LocalQueryBus
import you.dev.domain.restaurants.model.RestaurantDetails
import you.dev.domain.restaurants.query.AllRestaurantsQuery

private val queryLens = Body.auto<AllRestaurantsQuery>().toLens()
private val responseLens = Body.auto<List<RestaurantDetails>>().toLens()

class AllRestaurantsRoute(private val queryBus: LocalQueryBus) {
    private val spec = "/" meta {
        summary = "information details about a restaurant"
        receiving(queryLens to AllRestaurantsQuery())
        returning(
            OK, responseLens to listOf(
                RestaurantDetails.Info("r1", "Au bon coin", "au croisement du coin"),
                RestaurantDetails.Info("r2", "A l'autre coin", "juste en face du bon coin")
            )
        )
    } bindContract GET

    private val root: HttpHandler = { request: Request ->
        val query: AllRestaurantsQuery = queryLens(request)
        val response: List<RestaurantDetails> = queryBus.dispatch(query)
        Response(OK).with(responseLens of response)
    }

    operator fun invoke(): ContractRoute = spec to root
}
