package you.dev.domain.restaurants.route

import org.http4k.contract.ContractRoute
import org.http4k.contract.meta
import org.http4k.core.*
import org.http4k.core.Method.GET
import org.http4k.core.Status.Companion.OK
import org.http4k.format.Jackson.auto
import you.dev.core.query.local.LocalQueryBus
import you.dev.domain.restaurants.model.RestaurantDetails
import you.dev.domain.restaurants.query.RestaurantDetailsQuery

private val queryLens = Body.auto<RestaurantDetailsQuery>().toLens()
private val responseLens = Body.auto<RestaurantDetails>().toLens()

class RestaurantDetailsRoute(private val queryBus: LocalQueryBus) {
    private val spec = "/details" meta {
        summary = "information details about a restaurant"
        receiving(queryLens to RestaurantDetailsQuery("r1"))
        returning(OK, responseLens to RestaurantDetails.Info("r1", "Au bon coin", "au croisement du coin"))
    } bindContract GET

    private val details: HttpHandler = { request: Request ->
        val query: RestaurantDetailsQuery = queryLens(request)
        val response: RestaurantDetails = queryBus.dispatch(query)
        Response(OK).with(responseLens of response)
    }

    operator fun invoke(): ContractRoute = spec to details
}
