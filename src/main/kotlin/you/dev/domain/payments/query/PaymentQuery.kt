package you.dev.domain.payments.query

import you.dev.core.query.Query

open class PaymentQuery: Query {
    override fun getDomainName(): String = "payments"
}
