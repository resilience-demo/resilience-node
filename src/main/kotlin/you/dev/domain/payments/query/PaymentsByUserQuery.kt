package you.dev.domain.payments.query

data class PaymentsByUserQuery(val idUser: String) : PaymentQuery()
