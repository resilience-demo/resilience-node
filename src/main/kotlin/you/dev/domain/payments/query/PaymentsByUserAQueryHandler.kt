package you.dev.domain.payments.query

import you.dev.core.query.QueryHandler
import you.dev.domain.payments.model.Payment

class PaymentsByUserAQueryHandler : QueryHandler<PaymentsByUserQuery, List<Payment>> {
    private val allPayments: List<Payment> = listOf(
        Payment("u1", "r1", 119.0),
        Payment("u1", "r2", 132.0),
        Payment("u1", "r1", 46.0),
        Payment("u2", "r2", 34.4),
        Payment("u2", "r3", 34.2),
        Payment("u3", "r3", 234.7),
    )

    override fun execute(query: PaymentsByUserQuery): List<Payment> {
        return allPayments.filter { it.idUser == query.idUser }
    }
}
