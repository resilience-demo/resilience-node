package you.dev.domain.payments.route

import org.http4k.contract.contract
import org.http4k.contract.openapi.ApiInfo
import org.http4k.contract.openapi.v3.OpenApi3
import org.http4k.format.Jackson
import you.dev.core.query.local.LocalQueryBus

fun paymentRouter(queryBus: LocalQueryBus) = contract {
    renderer = OpenApi3(ApiInfo("Payments API", "v1.0"), Jackson)
    descriptionPath = "/swagger.json"
    routes += PaymentsByUserRoute(queryBus)()
}
