package you.dev.domain.payments.route

import org.http4k.contract.ContractRoute
import org.http4k.contract.meta
import org.http4k.core.*
import org.http4k.core.Method.GET
import org.http4k.core.Status.Companion.OK
import org.http4k.format.Jackson.auto
import you.dev.core.query.local.LocalQueryBus
import you.dev.domain.payments.model.Payment
import you.dev.domain.payments.query.PaymentsByUserQuery

private val queryLens = Body.auto<PaymentsByUserQuery>().toLens()
private val responseLens = Body.auto<List<Payment>>().toLens()

class PaymentsByUserRoute(private val queryBus: LocalQueryBus) {
    private val spec = "/by-user" meta {
        summary = "list of payments for a user"
        receiving(queryLens to PaymentsByUserQuery("u1"))
        returning(OK, responseLens to listOf(Payment("u1", "r1", 123.4)))
    } bindContract GET

    private val byUser: HttpHandler = { request: Request ->
        val query: PaymentsByUserQuery = queryLens(request)
        val response: List<Payment> = queryBus.dispatch(query)
        Response(OK).with(responseLens of response)
    }

    operator fun invoke(): ContractRoute = spec to byUser
}
