package you.dev.domain.payments.model

data class Payment(val idUser: String, val idRestaurant: String, val amount: Double)
