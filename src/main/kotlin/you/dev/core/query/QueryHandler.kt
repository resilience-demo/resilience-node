package you.dev.core.query

interface QueryHandler<T: Query, U> {
    fun execute(query: T) : U
}
