package you.dev.core.query.http

import org.http4k.core.*
import org.http4k.lens.BiDiBodyLens
import you.dev.core.query.Query
import you.dev.core.query.QueryHandler

class HttpQueryHandler<T : Query, U>(
    private val httpHandler: HttpHandler,
    private val domainName: String,
    private val path: String,
    private val method: Method,
    private val queryLens: BiDiBodyLens<T>,
    private val responseLens: BiDiBodyLens<U>,
) : QueryHandler<T, U> {

    override fun execute(query: T): U {
        val response =
            httpHandler(
                Request(method, "http://localhost:9000/$domainName$path")
                    .with(queryLens of query))
        return responseLens(response)
    }
}
