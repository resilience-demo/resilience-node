package you.dev.core.query.local

import you.dev.core.query.Query
import you.dev.core.query.QueryHandler
import kotlin.reflect.KClass

class LocalQueryBus {
    var handlers: MutableMap<KClass<*>, QueryHandler<Query, *>> = mutableMapOf()

    inline fun <reified T : Query> register(queryHandler: QueryHandler<T, *>) {
        handlers[T::class] = queryHandler as QueryHandler<Query, *>
    }

    inline fun <reified T: Query, U> dispatch(query: T) : U {
        return handlers[T::class]?.execute(query) as U
    }
}
