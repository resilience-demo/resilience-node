package you.dev

import you.dev.formats.JacksonMessage
import you.dev.formats.jacksonMessageLens
import you.dev.routes.ExampleContractRoute
import io.github.resilience4j.circuitbreaker.CircuitBreaker
import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig
import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig.SlidingWindowType.COUNT_BASED
import io.micrometer.core.instrument.simple.SimpleMeterRegistry
import java.time.Duration
import java.util.*
import org.http4k.client.JettyClient
import org.http4k.contract.bind
import org.http4k.contract.contract
import org.http4k.contract.openapi.ApiInfo
import org.http4k.contract.openapi.v3.OpenApi3
import org.http4k.contract.security.ApiKeySecurity
import org.http4k.core.Method.GET
import org.http4k.core.Method.POST
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status.Companion.INTERNAL_SERVER_ERROR
import org.http4k.core.Status.Companion.OK
import org.http4k.core.then
import org.http4k.core.with
import org.http4k.filter.ClientFilters
import org.http4k.filter.DebuggingFilters.PrintRequest
import org.http4k.filter.DebuggingFilters.PrintResponse
import org.http4k.filter.MicrometerMetrics
import org.http4k.filter.ResilienceFilters.CircuitBreak
import org.http4k.filter.ServerFilters
import org.http4k.format.Jackson
import org.http4k.lens.Query
import org.http4k.lens.int
import org.http4k.routing.bind
import org.http4k.routing.routes
import org.http4k.server.Jetty
import org.http4k.server.asServer
import you.dev.core.query.local.LocalQueryBus
import you.dev.domain.loyalties.query.DiscountsByUserQueryHandler
import you.dev.domain.loyalties.route.loyaltyRouter
import you.dev.domain.payments.query.PaymentsByUserAQueryHandler
import you.dev.domain.payments.route.paymentRouter
import you.dev.domain.restaurants.model.RestaurantsRepository
import you.dev.domain.restaurants.query.AllRestaurantsQueryHandler
import you.dev.domain.restaurants.query.RestaurantDetailsQueryHandler
import you.dev.domain.restaurants.route.restaurantRouter

// this is a micrometer registry used mostly for testing - substitute the correct implementation.
val registry = SimpleMeterRegistry()
val circuitBreaker = CircuitBreaker.of("circuit",
        CircuitBreakerConfig.custom()
                .slidingWindow(2, 2, COUNT_BASED)
                .permittedNumberOfCallsInHalfOpenState(2)
                .waitDurationInOpenState(Duration.ofSeconds(1))
                .build()
)

val circuitBreakerEndpointResponses = ArrayDeque<Response>().apply {
    add(Response(OK))
    add(Response(OK))
    add(Response(INTERNAL_SERVER_ERROR))
}

val queryBus = LocalQueryBus().apply {
    val repository = RestaurantsRepository()
    //registerContract(HttpQueryHandler("restaurants", "/", GET, AllRestaurantsQueryHandler(repository))
    register(AllRestaurantsQueryHandler(repository))
    register(RestaurantDetailsQueryHandler(repository))

    register(PaymentsByUserAQueryHandler())

    register(DiscountsByUserQueryHandler(this))
}

val app = routes(
    "/ping" bind GET to {
        Response(OK).body("pong")
    },

    "/formats/json/jackson" bind GET to {
        Response(OK).with(jacksonMessageLens of JacksonMessage("Barry", "Hello there!"))
    },

    "/testing/chaos" bind GET to {
        Response(OK).body("A normal response")
    },

    "/testing/kotest" bind GET to {request ->
        Response(OK).body("Echo '${request.bodyString()}'")
    },

    "/restaurants" bind restaurantRouter(queryBus),
    "/payments" bind paymentRouter(queryBus),
    "/loyalties" bind loyaltyRouter(queryBus),

    "/contract/api/v1" bind contract {
        renderer = OpenApi3(ApiInfo("ResilienceNode API", "v1.0"), Jackson)

        // Return Swagger API definition under /contract/api/v1/swagger.json
        descriptionPath = "/swagger.json"

        // You can use security filter tio protect routes
        security = ApiKeySecurity(Query.int().required("api"), { it == 42 }) // Allow only requests with &api=42

        // Add contract routes
        routes += ExampleContractRoute()
    },

    "/metrics" bind GET to {
        Response(OK).body("Example metrics route for ResilienceNode")
    },

    "/resilience" bind GET to {
        circuitBreakerEndpointResponses.pop()
    }
)

fun main() {

    val server = PrintRequest()
            .then(ServerFilters.MicrometerMetrics.RequestCounter(registry))
            .then(ServerFilters.MicrometerMetrics.RequestTimer(registry))
        .then(app)
        .asServer(Jetty(9000)).start()

    val client = PrintResponse()
            .then(ClientFilters.MicrometerMetrics.RequestCounter(registry))
            .then(ClientFilters.MicrometerMetrics.RequestTimer(registry))
            .then(CircuitBreak(circuitBreaker, isError = { r: Response -> !r.status.successful } ))
        .then(JettyClient())

    val response = client(Request(GET, "http://localhost:9000/ping"))

    println(response.bodyString())

    // Contract route example
    println(app(Request(GET, "/contract/api/v1/swagger.json")))
    println(app(Request(POST, "/contract/api/v1/echo")
            .query("api", "42")
            .body("""{"name":"Bob","message":"Hello"}""")))
    // Example usage of metrics
    // make some calls
    repeat(10) {
        app(Request(GET, "/metrics"))
        client(Request(GET, "https://http4k.org"))
    }

    // see some results
    registry.forEachMeter { println("${it.id} ${it.measure().joinToString(",")}") }
    // Resilience4j
    // Example of circuit breaker
    println("Result: " + client(Request(GET, "http://localhost:9000/resilience")).status + " Circuit is: " + circuitBreaker.state)
    println("Result: " + client(Request(GET, "http://localhost:9000/resilience")).status + " Circuit is: " + circuitBreaker.state)

    Thread.sleep(1100) // wait for reset

    println("Result: " + client(Request(GET, "http://localhost:9000/resilience")).status + " Circuit is: " + circuitBreaker.state)
    println("Result: " + client(Request(GET, "http://localhost:9000/resilience")).status + " Circuit is: " + circuitBreaker.state)

    println("Server started on " + server.port())
}
