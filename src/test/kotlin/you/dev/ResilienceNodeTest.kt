package you.dev

import org.http4k.chaos.ChaosBehaviours.ReturnStatus
import org.http4k.chaos.ChaosEngine
import org.http4k.chaos.ChaosStages.Wait
import org.http4k.chaos.ChaosTriggers.PercentageBased
import org.http4k.chaos.appliedWhen
import org.http4k.chaos.then
import org.http4k.chaos.until
import org.http4k.core.Method
import org.http4k.core.Method.GET
import org.http4k.core.Method.POST
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status.Companion.INTERNAL_SERVER_ERROR
import org.http4k.core.Status.Companion.OK
import org.http4k.core.then
import org.http4k.kotest.shouldHaveBody
import org.http4k.kotest.shouldHaveStatus
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ResilienceNodeTest {

    @Test
    fun `Ping test`() {
        assertEquals(app(Request(GET, "/ping")), Response(OK).body("pong"))
    }
    @Test
    fun `Check received correct response when chaos enabled then disabled`() {
        // chaos is split into "stages", which can be triggered by specific request or time-based criteria
        val doNothingStage = Wait.until { tx: Request -> tx.method == POST }
        val errorStage = ReturnStatus(INTERNAL_SERVER_ERROR).appliedWhen(PercentageBased(50))
    
        // chain the stages together with then() and create the Chaos Engine (activated)
        val engine = ChaosEngine(doNothingStage.then(errorStage)).enable()
    
        val appWithChaos = engine.then(app)
        fun performA(method: Method) = appWithChaos(Request(method, "/testing/chaos"))
    
        repeat(10) {
            assertEquals(performA(GET).status, OK)
        }
    
        // Trigger chaos
        performA(POST)
    
        repeat(10) {
            val response = performA(GET)
            println("Got ${response.status}")
    
        }
    
        // disable the chaos
        engine.disable()
    
        repeat(10) {
            assertEquals(performA(GET).status, OK)
        }
    }
    @Test
    fun `Check Kotest matcher for http4k work as expected`() {
        val request = Request(GET, "/testing/kotest?a=b").body("http4k is cool").header("my header", "a value")
    
        val response = app(request)
    
        // response assertions
        response shouldHaveStatus OK
        response shouldHaveBody "Echo 'http4k is cool'"
    }

}
